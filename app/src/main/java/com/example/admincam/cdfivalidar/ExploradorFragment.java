package com.example.admincam.cdfivalidar;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.admincam.cdfivalidar.model.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExploradorFragment extends ListFragment {

    private List<String> listaNombresArchivos;
    private List<String> listaRutasArchivos;
    private ArrayAdapter<String> adaptador;
    private String directorioRaiz;
    private TextView carpetaActual;
    private int Aux = 0;
    private TextView nombreArchivo, directorioActual;
    model fileName;
    ExploradorFragment exploradorFragment;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_explorador_lista, container, false);

        // Inflate the layout for this fragment


        nombreArchivo = (TextView)v.findViewById(R.id.nameFile);
        directorioActual = (TextView)v.findViewById(R.id.rutaActual);
        exploradorFragment = new ExploradorFragment();
        fileName = new model(getActivity());

        carpetaActual = (TextView)v.findViewById(R.id.rutaActual);

        //directorioRaiz = Environment.getExternalStorageDirectory().getPath();
        //verArchivosDirectorio(directorioRaiz);

        /** saber de donde tomar los archivos si de memoria externa o interna **/
        String typeMemorySelected = getArguments().getString("tipoMemoria");
        if(typeMemorySelected == "externa") {

            directorioRaiz = Environment.getExternalStorageDirectory().getPath();
        } else if(typeMemorySelected == "interna") {

            directorioRaiz = Environment.getRootDirectory().getPath();
        }
        directorioActual.setText(directorioRaiz);
        verArchivosDirectorio(directorioRaiz);

        /*String tipoMemoriaStr = getActivity().getIntent().getExtras().getString("interna");
        int tipoMemoria = Integer.parseInt(tipoMemoriaStr);
        if(tipoMemoria==1){
            directorioRaiz = Environment.getRootDirectory().getPath();
            verArchivosDirectorio(directorioRaiz);
        }else{
            directorioRaiz = Environment.getExternalStorageDirectory().getPath();
            verArchivosDirectorio(directorioRaiz);
        }
        */
        return v;
    }

    private void verArchivosDirectorio(String rutaDirectorio) {


        carpetaActual.setText("Estas en: " + rutaDirectorio);
        listaNombresArchivos = new ArrayList<String>();
        listaRutasArchivos = new ArrayList<String>();
        File directorioActual = new File(rutaDirectorio);
        File[] listaArchivos = directorioActual.listFiles();

        int x = 0;

        if (!rutaDirectorio.equals(directorioRaiz)){
            listaNombresArchivos.add("../");
            listaRutasArchivos.add(directorioActual.getParent());
            x = 1;
        }

        for (File archivo : listaArchivos){
            listaRutasArchivos.add(archivo.getPath());
        }

        Collections.sort(listaRutasArchivos, String.CASE_INSENSITIVE_ORDER);

        for (int i = x; i < listaRutasArchivos.size(); i++){
            File archivo = new File(listaRutasArchivos.get(i));
            if (archivo.isFile()) {
                listaNombresArchivos.add(archivo.getName());
            } else {
                listaNombresArchivos.add("/" + archivo.getName());
            }
        }

        if (listaArchivos.length < 1) {
            listaNombresArchivos.add("No hay ningun archivo");
            listaRutasArchivos.add(rutaDirectorio);
        }

        adaptador = new ArrayAdapter<String>(getActivity(), R.layout.fragment_explorador_item, listaNombresArchivos);
        setListAdapter(adaptador);
    }

    public void onListItemClick(ListView l, View v, int position, long id){

        /*
        if (Aux == 0){
            Aux = 1;
            if (position == 0){
                directorioRaiz = Environment.getRootDirectory().getPath();
                verArchivosDirectorio(directorioRaiz);

            } else{
                directorioRaiz = Environment.getExternalStorageDirectory().getPath();

                verArchivosDirectorio(directorioRaiz);
            }


        }else {
*/

            File archivo = new File(listaRutasArchivos.get(position));


            if (archivo.isFile()) {

                fileName.setNameFile(archivo.getName().toString(), archivo.getPath().toString());
                //finish();
                //String auxEdiNombre=nombreArchivo.getText().toString();
                /*Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("archivo",archivo.getName().toString());
                intent.putExtra("ruta",archivo.getPath().toString());
                startActivity(intent);*/

                //se obtienen datos del fragment para enviarlos
                Bundle args = new Bundle();
                args.putString("archivo",archivo.getName().toString());
                args.putString("ruta", archivo.getPath().toString());


                //instancia de la clase donde se van a mandar los datos obtenidos
                ValidaFragment miFrag = new ValidaFragment();
                miFrag.setArguments(args);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout,  miFrag);
                transaction.addToBackStack(null);
                transaction.commit();
            } else {

                verArchivosDirectorio(listaRutasArchivos.get(position));
            }
       // }

    }
}
