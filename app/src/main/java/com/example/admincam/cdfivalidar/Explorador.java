package com.example.admincam.cdfivalidar;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Environment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.example.admincam.cdfivalidar.model.model;

public class Explorador extends ListActivity  {

    private List<String> listaNombresArchivos;
    private List<String> listaRutasArchivos;
    private ArrayAdapter<String> adaptador;
    private String directorioRaiz;
    private TextView carpetaActual;
    private int Aux = 0;
    private TextView nombreArchivo;
    model fileName;
    MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorador);



        nombreArchivo = (TextView)findViewById(R.id.nameFile);
        mainActivity = new MainActivity();
        fileName = new model(this);

        carpetaActual = (TextView)findViewById(R.id.rutaActual);

        if(Integer.parseInt(getIntent().getExtras().get("interna").toString())==1){
            directorioRaiz = Environment.getRootDirectory().getPath();
            verArchivosDirectorio(directorioRaiz);
        }else{
            directorioRaiz = Environment.getExternalStorageDirectory().getPath();
            verArchivosDirectorio(directorioRaiz);
        }

        //directorioRaiz = Environment.getExternalStorageDirectory().getPath();
        /*if(Environment.isExternalStorageEmulated()){
            Aux = 0;
            listaNombresArchivos = new ArrayList<String>();
            //listaNombresArchivos.add("Memoria Interna");
            //listaNombresArchivos.add("Memoria Externa");

            adaptador = new ArrayAdapter<String>(this,
                    R.layout.lista_texto, listaNombresArchivos);
            setListAdapter(adaptador);
        }
        else{
            Aux = 1;
            directorioRaiz = Environment.getRootDirectory().getPath();

            verArchivosDirectorio(directorioRaiz);
        }*/
    }

    private void verArchivosDirectorio(String rutaDirectorio) {


        carpetaActual.setText("Estas en: " + rutaDirectorio);
        listaNombresArchivos = new ArrayList<String>();
        listaRutasArchivos = new ArrayList<String>();
        File directorioActual = new File(rutaDirectorio);
        File[] listaArchivos = directorioActual.listFiles();

        int x = 0;

        if (!rutaDirectorio.equals(directorioRaiz)){
            listaNombresArchivos.add("../");
            listaRutasArchivos.add(directorioActual.getParent());
            x = 1;
        }

        for (File archivo : listaArchivos){
            listaRutasArchivos.add(archivo.getPath());
        }

        Collections.sort(listaRutasArchivos, String.CASE_INSENSITIVE_ORDER);

        for (int i = x; i < listaRutasArchivos.size(); i++){
            File archivo = new File(listaRutasArchivos.get(i));
            if (archivo.isFile()) {
                listaNombresArchivos.add(archivo.getName());
            } else {
                listaNombresArchivos.add("/" + archivo.getName());
            }
        }

        if (listaArchivos.length < 1) {
            listaNombresArchivos.add("No hay ningun archivo");
            listaRutasArchivos.add(rutaDirectorio);
        }

        adaptador = new ArrayAdapter<String>(this,
                R.layout.lista_texto, listaNombresArchivos);
        setListAdapter(adaptador);
    }

    protected void onListItemClick(ListView l, View v, int position, long id){

        if (Aux == 0){
            Aux = 1;
            if (position == 0){
                directorioRaiz = Environment.getRootDirectory().getPath();
                verArchivosDirectorio(directorioRaiz);

            } else{
                directorioRaiz = Environment.getExternalStorageDirectory().getPath();

                verArchivosDirectorio(directorioRaiz);
            }


        }else {


            File archivo = new File(listaRutasArchivos.get(position));


            if (archivo.isFile()) {

                fileName.setNameFile(archivo.getName().toString(), archivo.getPath().toString());
                finish();
                //String auxEdiNombre=nombreArchivo.getText().toString();
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("archivo",archivo.getName().toString());
                intent.putExtra("ruta",archivo.getPath().toString());
                startActivity(intent);


                /*ArchivoCargadoFragment miFrag = new ArchivoCargadoFragment();
                Bundle args = new Bundle();
                args.putString("archivo",archivo.getName().toString());
                args.putString("ruta", archivo.getPath().toString());
                miFrag.setArguments(args);*/

            } else {

                verArchivosDirectorio(listaRutasArchivos.get(position));
            }
        }

    }
}


