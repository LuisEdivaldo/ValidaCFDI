package com.example.admincam.cdfivalidar;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.admincam.cdfivalidar.model.model;

/**
 * Este Fragment muestra los botonos con las memorias interna o externa, el cual al seleccionar
 * una de esta mostrara el explorador, el cual al terminar el explorador esta resivira el nombre
 * y ruta del archivo en forma de cadena, la cual iniciara el fragment de Validar con el nombre del
 * archivo para que lo muestre y la ruta para crear el archivo y validarlo
*/
public class SeleccionaMemoriaFragment extends Fragment implements View.OnClickListener {

    private static final int ABRIRFICHERO_RESULT_CODE = 0;


    private Button btMemoriaInterna;
    private Button btMemoriaExterna;

    private TextView nombreArchivo;

    public model fileName;
    public SeleccionaMemoriaFragment() {
        // Required empty public constructor
    }

    public static SeleccionaMemoriaFragment newInstance() {
        SeleccionaMemoriaFragment fragment = new SeleccionaMemoriaFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_gestor_archivos, container, false);

        btMemoriaInterna = (Button) v.findViewById(R.id.bt_memoria_interna);
        btMemoriaExterna = (Button) v.findViewById(R.id.bt_memoria_externa);

        nombreArchivo = (TextView) v.findViewById(R.id.nameFile);

        btMemoriaInterna.setOnClickListener(this);
        btMemoriaExterna.setOnClickListener(this);

        fileName = new model(getContext());
        fileName.setNameFile("Selecciona Archivo: ", "");

        return v;
    }

    /**
     * El metodo se ejecutara por seleccionar alguna memoria (interna o externa), el cual iniciara
     * un dialogo para explorar archivos
     **/
    @Override
    public void onClick(View v) {
        int viewId = v.getId();

        switch (viewId) {
            case R.id.bt_memoria_interna:
            /*Intent explorador = new Intent(getActivity(), Explorador.class);
            explorador.putExtra("interna", 1);
            startActivity(explorador);*/

                Fragment fragment = new ExploradorFragment();
                Bundle args = new Bundle();
                args.putString("tipoMemoria", "interna");
                fragment.setArguments(args);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout,  fragment);
                transaction.addToBackStack(null);
                transaction.commit();

                break;
            case R.id.bt_memoria_externa:
            /*Intent explorador1 = new Intent(getActivity(), Explorador.class);
            explorador1.putExtra("interna", 0);
            startActivity(explorador1);*/
                Fragment fragment1 = new ExploradorFragment();
                Bundle args1 = new Bundle();
                args1.putString("tipoMemoria", "externa");
                fragment1.setArguments(args1);
                FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                transaction1.replace(R.id.frame_layout,  fragment1);
                transaction1.addToBackStack(null);
                transaction1.commit();
                break;
        }
    }




   /**
     * El metodo se ejecutara cuando el usuario seleccione un archivo, asignando el nombre y
     * ruta del archivo seleccionado.
   **/
   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data) {
       switch (requestCode) {
           case ABRIRFICHERO_RESULT_CODE:
               if (resultCode == -1) {

                   // Mostramos por pantalla la ruta del archivo seleccionado.
                   String ruta = data.getData().getPath();
                   nombreArchivo.setText(ruta);
               }
       }
   }



}
