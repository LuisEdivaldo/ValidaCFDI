package com.example.admincam.cdfivalidar.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.admincam.cdfivalidar.R;

public class FileItemAdapter extends ArrayAdapter<String> {

    public Context context;
    public String[] nombreArchivos;
    public LayoutInflater layoutInflater;

    public FileItemAdapter(Context context, String[] nombreArchivos) {
        super(context, R.layout.fragment_explorador_item, nombreArchivos);

        this.context = context;
        this.nombreArchivos = nombreArchivos;

    }


    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.fragment_explorador_item, null);
        }

        TextView tvNombreArchivo = (TextView) convertView.findViewById(R.id.tv_nombre_archivo);

        tvNombreArchivo.setText(nombreArchivos[position]);

        return convertView;
    }





}
