package com.example.admincam.cdfivalidar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admincam.cdfivalidar.HTTPConection.WebServiceGet;
import com.example.admincam.cdfivalidar.HTTPConection.WebServicePost;
import com.example.admincam.cdfivalidar.model.model;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;

/**
 * Este Fragment mostrara el archivo seleccionado con su nombre, el cual debera llegar
 * a travez de donde este es creado,
**/

public class ValidaFragment extends Fragment {

    Button btValida;

    String nombreArchivo;
    TextView tvNombreArchivo;

    String rutaArchivo;



    public ValidaFragment() {
        // Required empty public constructor
    }

    public static ValidaFragment newInstance() {
        ValidaFragment fragment = new ValidaFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //resibimos los datos de nombre y ruta de archivo de donde fue invocado.
        Bundle argsIn = getArguments();
        this.nombreArchivo = argsIn.getString("archivo", null);
        this.rutaArchivo = argsIn.getString("ruta", null);
        fileName = new model(getContext());
        fileName.setNameFile(this.nombreArchivo, this.rutaArchivo); //para que la validacion se haga
        Log.d("aa", "Resultado explorador: Ruta: " + this.nombreArchivo + " - Nombre: " + this.rutaArchivo);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_valida, container, false);

        this.tvNombreArchivo = (TextView) v.findViewById(R.id.tv_nombre_archivo);
        this.tvNombreArchivo.setText(this.nombreArchivo);

        this.btValida = (Button) v.findViewById(R.id.bt_validar);
        this.btValida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBody();
                body = "";
            }
        });
        return v;
    }

    //#####################################################
    public String body = "";
    private String Timbre;
    private String Nombre;
    private String Rfc;
    public model fileName;

    public void sendBody(){
        if(ValidatePing()){
            if(validateFile()){
                String request = ValidaXML();

                //Creamos fragment y le enviamos informacion para que construlla la vista con la informacion resultante
                RespuestaFragment fragment = RespuestaFragment.newInstance();
                Bundle args = new Bundle();
                args.putString("nombre", Nombre);
                args.putString("rfc", Rfc);
                args.putString("nombre_archivo", fileName.getNameFile());
                fragment.setArguments(args);

                //Le mostramos al usuario la nueva informacion
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack("xyz");
                fragmentTransaction.hide(ValidaFragment.this);
                fragmentTransaction.add(R.id.frame_layout, fragment);
                fragmentTransaction.commit();

            }
            else{
                Toast.makeText(getContext(), body, Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getContext(), "No hay conexiòn con el servidor", Toast.LENGTH_LONG).show();
        }
    }

    private boolean ValidatePing(){
        String rstWS = null;
        String[] parametro = {};
        String[] valor = {};
        String url = "http://189.204.7.188:4000/api/v1/ping/success";
        boolean conection = false;

        WebServiceGet WS = new WebServiceGet(getContext(), url, parametro.length, parametro, valor);

        try {
            rstWS = WS.execute().get();
            JSONObject objeto  = null;

            try {
                objeto = new JSONObject(rstWS);
                if(objeto.has("estatus")) {
                    //get Value of video
                    String value = objeto.optString("estatus");
                    if (value.contentEquals("El archivo se validó correctamente")) {
                        conection = true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return conection;
    }

    private boolean validateFile() {
        String file = fileName.getNameFile();
        String[] files = file.split("\\.");
        boolean ValidateBody = false;
        if(files.length > 1){
            if (files[1].equals("xml")) {
                try
                {
                    fileName.getPathFile();
                    File f = new File(fileName.getPathFile());
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            new FileInputStream(f)));
                    String linea = fin.readLine();
                    while(linea!=null){
                        body = body + linea;
                        linea = fin.readLine();
                    }
                    fin.close();

                            /*body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                                    "<cfdi:Comprobante xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Version=\"3.3\" Fecha=\"2016-11-17T13:57:06\" Sello=\"temporal\" NoCertificado=\"20001000000100005867\" Certificado=\"temporal\" SubTotal=\"88440.00\" Moneda=\"MXN\" Total=\"88440.00\" TipoDeComprobante=\"E\" LugarExpedicion=\"64000\">\n" +
                                    "<cfdi:Emisor Rfc=\"AAA010101AAA\" RegimenFiscal=\"601\" />\n" +
                                    "<cfdi:Receptor Rfc=\"AAA010101AAA\" UsoCFDI=\"G01\" />\n" +
                                    "<cfdi:Conceptos>\n" +
                                    "<cfdi:Concepto ClaveProdServ=\"01010101\" Cantidad=\"1\" ClaveUnidad=\"ACT\" Descripcion=\"Pago de nómina\" ValorUnitario=\"85240.00\" Importe=\"85240.00\" />\n" +
                                    "<cfdi:Concepto ClaveProdServ=\"01010101\" Cantidad=\"8\" ClaveUnidad=\"ACT\" Descripcion=\"Pago de nómina horas extra\" ValorUnitario=\"400.00\" Importe=\"3200.00\" />\n" +
                                    "</cfdi:Conceptos>\n" +
                                    "</cfdi:Comprobante>";*/

                    JSONObject jsonCFDI = null;
                    try {
                        jsonCFDI = XML.toJSONObject(body);

                        if(jsonCFDI.has("cfdi:Comprobante")){
                            JSONObject jsonComprobante = new JSONObject(jsonCFDI.get("cfdi:Comprobante").toString());
                            if(jsonComprobante.getString("Version").equals("3.3")) {
                                if(jsonComprobante.get("Certificado").equals("temporal")){
                                    Timbre = "cfdi_3.3_sin_timbre";
                                    JSONObject jsonReceptor = new JSONObject(jsonComprobante.get("cfdi:Receptor").toString());
                                    Nombre = jsonReceptor.get("Nombre").toString();
                                    Rfc = jsonReceptor.get("Rfc").toString();
                                }
                                else{
                                    Timbre = "cfdi_3.3_con_timbre";
                                    JSONObject jsonReceptor = new JSONObject(jsonComprobante.get("cfdi:Receptor").toString());
                                    Nombre = jsonReceptor.get("Nombre").toString();
                                    Rfc = jsonReceptor.get("Rfc").toString();

                                }
//                                Toast.makeText(this, jsonComprobante.getString("Version"), Toast.LENGTH_LONG).show();
                                ValidateBody = true;
                            }
                            else{
                                body = "la version no es la correcta!";
                            }
                        }
                        else{
                            body = "No es una factura electronica!";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        body = "Error al leer el fichero xml!";
                    }
                }
                catch (Exception ex)
                {
                    body = "Error al leer el fichero xml!";
                }
            }
            else{
                body = "No es un archivo xml!";
            }
        }
        else{
            body = "Debe Seleccionar un archivo!";
        }

        return ValidateBody;
    }

    public String ValidaXML(){
        String rstWS = "";
        String url = "http://189.204.7.188:4000/cfdi/validar";


        WebServicePost WSt = new WebServicePost(getContext(), url, Timbre, body);

        try {
            rstWS = WSt.execute().get();
        } catch (InterruptedException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        Toast.makeText(this, rstWS, Toast.LENGTH_LONG).show();
        return rstWS;
    }
}
