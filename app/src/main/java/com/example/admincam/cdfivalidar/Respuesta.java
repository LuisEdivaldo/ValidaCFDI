package com.example.admincam.cdfivalidar;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import com.example.admincam.cdfivalidar.model.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Respuesta extends AppCompatActivity {

    public FloatingActionButton regresar;
    public TextView nombre, rfc, answer, nombreArchivo;
    Intent intent;
    public model fileName;
    public String Nombre;
    public String Rfc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respuesta);
        getSupportActionBar().hide();

        intent = getIntent();

        String body = intent.getStringExtra("body");
        Nombre = intent.getStringExtra("nombre");
        Rfc = intent.getStringExtra("rfc");

        init();
        fileName = new model(this);
        viewRequest(body);


        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(Respuesta.this, Inicio.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        nombreArchivo.setText(fileName.getNameFile());
    }

    private void viewRequest(String body) {
        try {
            JSONObject JSONbody = new JSONObject(body);
            if(JSONbody.has("estatus")){
                answer.setText(JSONbody.getString("estatus"));
                nombre.setText(this.Nombre);
                rfc.setText(this.Rfc);
            }
            else if(JSONbody.has("mensaje")){
                answer.setText(JSONbody.getString("mensaje"));
                nombre.setText(this.Nombre);
                rfc.setText(this.Rfc);
            }
            else{
                answer.setText(body);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            answer.setText(body);
        }
    }


    public void init() {
        regresar = (FloatingActionButton) findViewById(R.id.btnBack);
        nombre = (TextView) findViewById(R.id.txtName);
        rfc = (TextView) findViewById(R.id.txtRFC);
        answer = (TextView) findViewById(R.id.txtAnswer);
        nombreArchivo = (TextView) findViewById(R.id.nameFile);

    }

}
