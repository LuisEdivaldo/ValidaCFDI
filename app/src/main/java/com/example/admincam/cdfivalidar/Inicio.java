package com.example.admincam.cdfivalidar;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Inicio extends AppCompatActivity {

    private FloatingActionButton botonExplorador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        getSupportActionBar().hide();

        init ();

        botonExplorador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Inicio.this, GestorArchivos.class);
                startActivity(intent);
            }
        });

    }

    private void init() {

        botonExplorador = (FloatingActionButton)findViewById(R.id.btnNuevoAechivo);
    }
}
