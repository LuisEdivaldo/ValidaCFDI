package com.example.admincam.cdfivalidar;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RespuestaFragment extends Fragment {

    TextView tvNombre;
    TextView tvRfc;
    TextView tvNombreArchivo;

    String nombre;
    String rfc;
    String nombreAechivo;


    public RespuestaFragment() {
        // Required empty public constructor
    }

    public static RespuestaFragment newInstance() {
        RespuestaFragment fragment = new RespuestaFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //campturamos informacion enviada desde donde fue llamado el fragment
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.nombre = bundle.getString("nombre", "vacio");
            this.rfc = bundle.getString("rfc", "vacio");
            this.nombreAechivo = bundle.getString("nombre_archivo", "vacio");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_respuesta, container, false);

        tvNombre = (TextView)  v.findViewById(R.id.tv_nombre);
        tvNombre.setText(this.nombre);

        tvRfc = (TextView)  v.findViewById(R.id.tv_rfc);
        tvRfc.setText(this.rfc);


        tvNombreArchivo = (TextView) v.findViewById(R.id.tv_nombre_archivo_resp);
        tvNombreArchivo.setText(this.nombreAechivo);


        return v;
    }

}
