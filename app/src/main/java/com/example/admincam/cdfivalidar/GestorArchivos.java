package com.example.admincam.cdfivalidar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.admincam.cdfivalidar.model.model;

public class GestorArchivos extends AppCompatActivity implements View.OnClickListener {

    private static final int ABRIRFICHERO_RESULT_CODE = 0;

    public Button interna, externa;
    public model fileName;
    public TextView nombreArchivo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestor_archivos);
        getSupportActionBar().hide();

        init();
        fileName = new model(this);
        fileName.setNameFile("Seleccione Archivo!", "");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ABRIRFICHERO_RESULT_CODE:
                if (resultCode == RESULT_OK) {

                    // Mostramos por pantalla la ruta del archivo seleccionado.
                    String ruta = data.getData().getPath();
                    nombreArchivo.setText(ruta);
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        nombreArchivo.setText(fileName.getNameFile());
    }

    public void init(){

        interna = (Button)findViewById(R.id.btnInterna);
        externa = (Button)findViewById(R.id.btnExterna);
        nombreArchivo = (TextView) findViewById(R.id.nameFile);


        interna.setOnClickListener(this);
        externa.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();

        switch (viewId) {
            case R.id.btnInterna:
                Intent explorador = new Intent(this, Explorador.class);
                explorador.putExtra("interna",1);
                startActivity(explorador);
                break;
            case R.id.btnExterna:
                Intent explorador1 = new Intent(this, Explorador.class);
                explorador1.putExtra("interna",0);
                startActivity(explorador1);
                break;
        }
    }


}
