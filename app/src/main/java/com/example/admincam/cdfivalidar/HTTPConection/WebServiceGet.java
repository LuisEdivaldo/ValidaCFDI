package com.example.admincam.cdfivalidar.HTTPConection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;


import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.content.ContentValues.TAG;

/**
 * Clase que permite conectar a distintos WebServices
 * @author ces
 *
 */
public class WebServiceGet extends AsyncTask <String, Void, String>
{
	//Declaracion de variables de ambito global
	Context contextoEjecucion;
	String url;
	int nParametros;
	String parametro[];
	String valor[];
	String peticion="";
	String rstWebPage="";
	private ProgressDialog dialog;
	AlertDialog alert;
	
//	public static Activity;
	
	/**
	 * Constructor de la clase
	 * @param c
	 * @param url
	 * @param nParametros
	 * @param parametro
	 * @param valor
	*/
	public WebServiceGet(Context c, String url, int nParametros, String[] parametro, String[] valor)
	{
		// TODO Auto-generated constructor stub
		contextoEjecucion = c;
		this.url=url;
		this.nParametros=nParametros;
		this.parametro=parametro;
		this.valor=valor;
		dialog = new ProgressDialog(contextoEjecucion);
		
	}
	@Override 
    protected  void onPreExecute ()  {

		
    }
		
	@Override
	protected String doInBackground(String... param) 
	{
		try 
		{
			StringBuilder sb = new StringBuilder();
//			for (int i = 0; i<this.nParametros; i++) {
//				sb.append(this.parametro[i] + "=" + this.valor[i]);
//				if (i < this.nParametros - 1) {
//					sb.append("&");
//				}
//			}

			URL url;
			try {
				url = new URL(this.url);

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");




				conn.setReadTimeout(3000);
				conn.setConnectTimeout(3000);
				conn.setDoInput(true);

				int responseCode=conn.getResponseCode();
				Log.d(TAG, "doInBackground: "+conn.getResponseMessage());

				if (responseCode == 200) {
					String line;
					BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
					while ((line=br.readLine()) != null) {
						rstWebPage+=line;
					}
				}

			} catch (Exception e) {
				rstWebPage="\"transaction_id\":\"\", \"estatus\":\"Sin Conexiòn\"}";
			}
		} 
		catch (Exception e) 
		{
			rstWebPage="\"transaction_id\":\"\", \"estatus\":\"Sin Conexiòn\"}";
		}
		return rstWebPage;
	}

    @Override
	protected void onProgressUpdate(Void... progr) {
        // TODO Update your ProgressBar here
    }
	
  
 @Override
 protected void onPostExecute(String result) {

		
 }

}
