package com.example.admincam.cdfivalidar;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class InicioFragment extends Fragment {
    private static final String LOG_TAG = "selecting_file";
    private FloatingActionButton botonExplorador;

    public static InicioFragment newInstance() {
        InicioFragment fragment = new InicioFragment();
        return fragment;
    }

    public InicioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_inicio, container, false);

        botonExplorador = (FloatingActionButton)v.findViewById(R.id.btnNuevoAechivo);
        botonExplorador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Muestra los botones para seleccionar una memoria interna o externa

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, SeleccionaMemoriaFragment.newInstance());
                fragmentTransaction.addToBackStack("xyz");
                fragmentTransaction.commit();
            }
        });
        return v;
    }
}
