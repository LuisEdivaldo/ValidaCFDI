package com.example.admincam.cdfivalidar.db;

/**
 * Created by admincam on 25/10/17.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by AlbertoLópez on 23/09/16.
 */
public class DataBase extends SQLiteOpenHelper {

    private Context context;

    public DataBase(Context context)
    {
        super(context, "dbFiles.db", null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String sql = "CREATE TABLE nameFile (id_File Integer, name Text, path Text);";
        db.execSQL(sql);

        db.execSQL("insert into nameFile values (1, '', '')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


}
