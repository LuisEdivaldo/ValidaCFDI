package com.example.admincam.cdfivalidar.model;

/**
 * Created by admincam on 25/10/17.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.example.admincam.cdfivalidar.db.DataBase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class model {
    Context contextoEjecucion;
    DataBase creador;
    SQLiteDatabase mDatos;

    /**
     * Se declara el constructor de la clase
     */
    public model(Context c) {
        // TODO Auto-generated constructor stub
        contextoEjecucion = c;
    }

    /**
     * Metodo que abre la conexion a la base de datos
     *
     * @return
     */
    public model abrirSqlite() {
        creador = new DataBase(contextoEjecucion);
        mDatos = creador.getWritableDatabase();
        return this;
    }

    /**
     * Metodo que cierra la conexion
     */
    public void cerrarSqlite() {
        creador.close();
    }

    public void setNameFile(String nameFile, String pathFile){
        abrirSqlite();
        ContentValues c = new ContentValues();
        c.put("name", nameFile);
        c.put("path", pathFile);

        mDatos.update("nameFile", c, "id_File = 1", null);
        cerrarSqlite();
    }

    public String getNameFile(){
        String nameFile="";

        creador = new DataBase(contextoEjecucion);
        mDatos = creador.getReadableDatabase();
        String sql = ("Select name from nameFile where id_File = 1");
        Cursor cur = mDatos.rawQuery(sql, null);

        if(cur.moveToFirst()){
            do{
                nameFile=cur.getString(0);
            }while(cur.moveToNext());
        }

        cerrarSqlite();

        return nameFile;
    }

    public String getPathFile(){
        String pathFile="";

        creador = new DataBase(contextoEjecucion);
        mDatos = creador.getReadableDatabase();
        String sql = ("Select path from nameFile where id_File = 1");
        Cursor cur = mDatos.rawQuery(sql, null);

        if(cur.moveToFirst()){
            do{
                pathFile=cur.getString(0);
            }while(cur.moveToNext());
        }

        cerrarSqlite();

        return pathFile;
    }
}