package com.example.admincam.cdfivalidar;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ContactanosFragment extends Fragment {

    private EditText nombre;
    private EditText email;
    private EditText mensaje;
    private Button enviar, llamar;

    public static ContactanosFragment newInstance() {
        ContactanosFragment fragment = new ContactanosFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contactanos, container, false);

        //nombre = (EditText)v.findViewById(R.id.txtNombre);
        //email = (EditText)v.findViewById(R.id.txtEmail);
        //mensaje = (EditText)v.findViewById(R.id.txtMensaje);
        enviar = (Button)v.findViewById(R.id.btnEnviarCorreo);
        llamar = (Button)v.findViewById(R.id.btnLlamar);


       enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail();
            }
        });

        llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        });

        return v;
    }



    private void sendEmail() {
        String[] TO = {"contacto@valida20.com"}; //aquí pon tu correo
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);

        // Esto podrás modificarlo si quieres, el asunto y el cuerpo del mensaje
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contactó");

        //emailIntent.putExtra(Intent.EXTRA_TEXT, "Mi nombre es : "+nombre.getText().toString()+mensaje.getText().toString()+" contactame en : "+email.getText().toString());
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Equipo de Valida20:\n" +" \n" +
                "Me gustaría recibir más información del producto, tenemos principal interés en generar validaciones de un gran volumen de documentos CFDI 3.3.\n" + " \n"+
                "Quedamos a la espera de su información.\n" + "\n" + "saludos.");
        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show();
        }
    }

    private void call (){
        Intent intentCall = new Intent(android.content.Intent.ACTION_DIAL, Uri.parse("tel:55 5570 5100")); //
        startActivity(intentCall);
    }
}
