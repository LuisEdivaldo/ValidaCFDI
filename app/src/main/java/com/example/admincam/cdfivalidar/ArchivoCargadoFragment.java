package com.example.admincam.cdfivalidar;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admincam.cdfivalidar.HTTPConection.WebServiceGet;
import com.example.admincam.cdfivalidar.HTTPConection.WebServicePost;
import com.example.admincam.cdfivalidar.model.model;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;


/**
 * A simple {@link Fragment} subclass.
 */
public class ArchivoCargadoFragment extends Fragment implements View.OnClickListener {

    private static final int ABRIRFICHERO_RESULT_CODE = 0;


    public TextView nombreArchivo;
    public Button enviar;
    public model fileName;
    public String body = "";
    Intent intent;
    private String Timbre;
    private String Nombre;
    private String Rfc;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_archivo_cargado, container, false);

        nombreArchivo = (TextView)v.findViewById(R.id.nameFile);
        enviar = (Button)v.findViewById(R.id.btnSend);

        //selArchvio.setOnClickListener(this);
        enviar.setOnClickListener(this);
        // Inflate the layout for this fragment


        return v;

    }


    @Override
    public void onClick(View v) {
        int viewId = v.getId();

        switch (viewId) {
            case R.id.btnSend:
                sendBody();
                body = "";
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        nombreArchivo.setText(fileName.getNameFile());
        nombreArchivo.setText(intent.getStringExtra("archivo").toString());
    }

    public void sendBody(){
        if(ValidatePing()){
            if(validateFile()){
                String request = ValidaXML();
                //finish();
                Intent requestActivity = new Intent(getActivity(), Respuesta.class);
                requestActivity.putExtra("body", request);
                requestActivity.putExtra("timbre", Timbre);
                requestActivity.putExtra("nombre", Nombre);
                requestActivity.putExtra("rfc", Rfc);
                startActivity(requestActivity);
            }
            else{
                Toast.makeText(getActivity(), body, Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getActivity(), "No hay conexiòn con el servidor", Toast.LENGTH_LONG).show();
        }
    }

    private boolean ValidatePing(){
        String rstWS = null;
        String[] parametro = {};
        String[] valor = {};
        String url = "http://189.204.7.188:4000/api/v1/ping/success";
        boolean conection = false;

        WebServiceGet WS = new WebServiceGet(getActivity(), url, parametro.length, parametro, valor);

        try {
            rstWS = WS.execute().get();
            JSONObject objeto  = null;

            try {
                objeto = new JSONObject(rstWS);
                if(objeto.has("estatus")) {
                    //get Value of video
                    String value = objeto.optString("estatus");
                    if (value.contentEquals("El archivo se validó correctamente")) {
                        conection = true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return conection;
    }

    private boolean validateFile() {
        String file = fileName.getNameFile();
        String[] files = file.split("\\.");
        boolean ValidateBody = false;
        if(files.length > 1){
            if (files[1].equals("xml")) {
                try
                {
                    fileName.getPathFile();
                    File f = new File(fileName.getPathFile());
                    BufferedReader fin =
                            new BufferedReader(
                                    new InputStreamReader(
                                            new FileInputStream(f)));
                    String linea = fin.readLine();
                    while(linea!=null){
                        body = body + linea;
                        linea = fin.readLine();
                    }
                    fin.close();

                            /*body = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                                    "<cfdi:Comprobante xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Version=\"3.3\" Fecha=\"2016-11-17T13:57:06\" Sello=\"temporal\" NoCertificado=\"20001000000100005867\" Certificado=\"temporal\" SubTotal=\"88440.00\" Moneda=\"MXN\" Total=\"88440.00\" TipoDeComprobante=\"E\" LugarExpedicion=\"64000\">\n" +
                                    "<cfdi:Emisor Rfc=\"AAA010101AAA\" RegimenFiscal=\"601\" />\n" +
                                    "<cfdi:Receptor Rfc=\"AAA010101AAA\" UsoCFDI=\"G01\" />\n" +
                                    "<cfdi:Conceptos>\n" +
                                    "<cfdi:Concepto ClaveProdServ=\"01010101\" Cantidad=\"1\" ClaveUnidad=\"ACT\" Descripcion=\"Pago de nómina\" ValorUnitario=\"85240.00\" Importe=\"85240.00\" />\n" +
                                    "<cfdi:Concepto ClaveProdServ=\"01010101\" Cantidad=\"8\" ClaveUnidad=\"ACT\" Descripcion=\"Pago de nómina horas extra\" ValorUnitario=\"400.00\" Importe=\"3200.00\" />\n" +
                                    "</cfdi:Conceptos>\n" +
                                    "</cfdi:Comprobante>";*/

                    JSONObject jsonCFDI = null;
                    try {
                        jsonCFDI = XML.toJSONObject(body);

                        if(jsonCFDI.has("cfdi:Comprobante")){
                            JSONObject jsonComprobante = new JSONObject(jsonCFDI.get("cfdi:Comprobante").toString());
                            if(jsonComprobante.getString("Version").equals("3.3")) {
                                if(jsonComprobante.get("Certificado").equals("temporal")){
                                    Timbre = "cfdi_3.3_sin_timbre";
                                    JSONObject jsonReceptor = new JSONObject(jsonComprobante.get("cfdi:Receptor").toString());
                                    Nombre = jsonReceptor.get("Nombre").toString();
                                    Rfc = jsonReceptor.get("Rfc").toString();
                                }
                                else{
                                    Timbre = "cfdi_3.3_con_timbre";
                                    JSONObject jsonReceptor = new JSONObject(jsonComprobante.get("cfdi:Receptor").toString());
                                    Nombre = jsonReceptor.get("Nombre").toString();
                                    Rfc = jsonReceptor.get("Rfc").toString();

                                }
//                                Toast.makeText(this, jsonComprobante.getString("Version"), Toast.LENGTH_LONG).show();
                                ValidateBody = true;
                            }
                            else{
                                body = "la version no es la correcta!";
                            }
                        }
                        else{
                            body = "No es una factura electronica!";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        body = "Error al leer el fichero xml!";
                    }
                }
                catch (Exception ex)
                {
                    body = "Error al leer el fichero xml!";
                }
            }
            else{
                body = "No es un archivo xml!";
            }
        }
        else{
            body = "Debe Seleccionar un archivo!";
        }

        return ValidateBody;
    }

    public String ValidaXML(){
        String rstWS = "";
        String url = "http://189.204.7.188:4000/cfdi/validar";


        WebServicePost WSt = new WebServicePost(getActivity(), url, Timbre, body);

        try {
            rstWS = WSt.execute().get();
        } catch (InterruptedException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {    // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        Toast.makeText(this, rstWS, Toast.LENGTH_LONG).show();
        return rstWS;
    }
}
