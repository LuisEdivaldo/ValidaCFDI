package com.example.admincam.cdfivalidar;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class MenuHome extends AppCompatActivity {

    private ViewPager viewPager;
    private CircleIndicator indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_home);
        getSupportActionBar().hide();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setUpViewpager();

        indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;

                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                selectedFragment = InformacionPrincipalFragment.newInstance();
                                break;
                            case R.id.action_item2:
                                selectedFragment = InicioFragment.newInstance();
                                break;
                            case R.id.action_item3:
                                selectedFragment = ContactanosFragment.newInstance();
                                break;
                        }

                        //limpiamos el stack de fragments para que no se ensime en otras vistas
                            FragmentManager fm = getSupportFragmentManager();
                            for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                                fm.popBackStack();
                            }


                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();

                        if(item.getItemId() == R.id.action_item1) {
                            indicator.setVisibility(View.VISIBLE);
                            viewPager.setVisibility(View.VISIBLE);
                            setUpViewpager();
                        } else{
                            indicator.setVisibility(View.GONE);
                            viewPager.setVisibility(View.GONE);
                        }

                        return true;
                    }
                });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.frame_layout, InformacionPrincipalFragment.newInstance());
        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);



    }

    private ArrayList<Fragment> agregandoFragments() {
        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(new InformacionPrincipalFragment());//¿Que es?
        fragments.add(new BeneficiosFragment());//Beneficios
        fragments.add(new TecnologiaFragment());//Tecnologua
        fragments.add(new TecnologiaFragment1());//Por que elegir
        return fragments;
    }

    private void setUpViewpager(){
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager(), agregandoFragments()));
        //tabLayout.setupWithViewPager(viewPager);
    }

}
