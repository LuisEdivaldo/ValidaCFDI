package com.example.admincam.cdfivalidar.HTTPConection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Clase que permite conectar a distintos WebServices
 * @author ces
 *
 */
public class WebServicePost extends AsyncTask <String, Void, String>
{
	//Declaracion de variables de ambito global
	Context contextoEjecucion;
	String url;
	String body;
	String scTimbre;
	String rstWebPage="";
	private ProgressDialog dialog;
	AlertDialog alert;

//	public static Activity;

	/**
	 * Constructor de la clase
	 * @param c
	 * @param url
	 * @param scTimbre
	 * @param body
	*/
	public WebServicePost(Context c, String url, String scTimbre, String body)
	{
		// TODO Auto-generated constructor stub
		contextoEjecucion = c;
		this.url=url;
		this.scTimbre = scTimbre;
		this.body = body;

		dialog = new ProgressDialog(contextoEjecucion);
		
	}
	@Override 
    protected  void onPreExecute ()  {

		
    }
		
	@Override
	protected String doInBackground(String... param) 
	{
		try 
		{
			StringBuilder sb = new StringBuilder();

			URL url;
			try {
				url = new URL(this.url);

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");

				conn.setRequestProperty("x-tipo-cfdi", scTimbre);
				conn.setRequestProperty("Content-Type", "text/plain; cherset=utf-8");
				conn.setRequestProperty("x-transaction-id", "21");


				conn.setReadTimeout(3000);
				conn.setConnectTimeout(3000);
				conn.setDoOutput(true);



				OutputStream os = conn.getOutputStream();
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(os, "UTF-8"));
				writer.write(body);
				writer.flush();
				writer.close();
				os.close();

				Log.d(TAG, "doInBackground: "+conn.getHeaderFields());

				int responseCode=conn.getResponseCode();

				if (responseCode == 200) {
					String line;
					BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
					while ((line=br.readLine()) != null) {
						rstWebPage+=line;
					}
				}
				else {
					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
					String line;
					while ((line=br.readLine()) != null) {
						rstWebPage+=line;
					}
				}

			} catch (Exception e) {
				rstWebPage="Error: "+e.getMessage();
			}
		} 
		catch (Exception e)
		{
			rstWebPage="Error: "+e.getMessage();
		}
		return rstWebPage;
	}

    @Override
    protected void onProgressUpdate(Void... progr) {
        // TODO Update your ProgressBar here
    }
	
  
 @Override
 protected void onPostExecute(String result) {

 }

}
