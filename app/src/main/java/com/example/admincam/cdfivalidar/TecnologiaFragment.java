package com.example.admincam.cdfivalidar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by gonet_cam on 28/11/17.
 */

public class TecnologiaFragment extends Fragment {

    public static InformacionPrincipalFragment newInstance() {
        InformacionPrincipalFragment fragment = new InformacionPrincipalFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tecnologia, container, false);
    }

}
